#include "LedStrip.h"

LedStrip::LedStrip(int pNumOfLeds, int pSizeOfLed, int pMargin) {
    this->numOfLeds = pNumOfLeds;
    this->sizeOfLed = pSizeOfLed;
    this->margin = pMargin;
    for (; pNumOfLeds > 0; pNumOfLeds--) {
        this->leds.push_back(new Color());
    }
}
