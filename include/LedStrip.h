#ifndef LED_STRIP_H
#define LED_STRIP_H

#include <list>

#include "Color.h"

class LedStrip {
private:
    int numOfLeds;
    int sizeOfLed;
    int margin;
    std::list<Color*> leds;
public:
    LedStrip(int pNumOfLeds, int pSizeOfLed, int pMargin);
};

#endif
