#ifndef COLOR_H
#define COLOR_H

#include <cstdint>

class Color {
private:
    uint8_t red;
    uint8_t green;
    uint8_t blue;
public:
    Color();
    Color(uint8_t pRed, uint8_t pGreen, uint8_t pBlue);
};

#endif
